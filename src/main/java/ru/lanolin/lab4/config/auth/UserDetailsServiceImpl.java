package ru.lanolin.lab4.config.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.lanolin.lab4.domain.User;
import ru.lanolin.lab4.exception.NotFoundException;
import ru.lanolin.lab4.repo.UserRepo;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private final UserRepo userRepo;

	@Autowired
	public UserDetailsServiceImpl(UserRepo userRepo) {this.userRepo = userRepo;}

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepo.findByLogin(username).orElseThrow(() -> new UsernameNotFoundException(username));

		Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>(){{
			add(new SimpleGrantedAuthority("USER"));
		}};

		return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), grantedAuthorities);
	}
}
