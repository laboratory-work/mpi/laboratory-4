package ru.lanolin.lab4.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.RepositoryDefinition;
import ru.lanolin.lab4.domain.Point;
import ru.lanolin.lab4.domain.User;

import java.util.List;

@RepositoryDefinition(domainClass = Point.class, idClass = Long.class)
public interface PointRepo extends JpaRepository<Point, Long> {

}
